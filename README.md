# Scripts for Collectd Exec plugin #

A collection of simple extensions for the Exec plugin of [Collectd](https://collectd.org/)

Please read official documentation for Exec plugin [here](https://www.collectd.org/wiki/index.php/Plugin:Exec), [here](https://collectd.org/documentation/manpages/collectd-exec.5.shtml) and [here](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_exec) 

____
____


## top.sh ##

Get top processes by CPU and MEM usage.    

#### Dependencies ####

    bash (v4+ is fine)
    top
    gnu awk
    gnu grep
    tail
    bc

#### Usage via command line ####

    /path/to/top.sh [-i TOP_ITERATIONS] [-d TOP_ITER_DELAY] [-t TOP_MAX_TASKS]

#### Options ####

    TOP_ITERATIONS: 'top' loops to get useful values, default to 2 (1 is not enough, 3 or more probably unhelpful)
    TOP_ITER_DELAY: 'top' delay in seconds between iterations, default to 2
    TOP_MAX_TASKS: maximum tasks to get from 'top' output, default to 10

#### Environment variables ####

    COLLECTD_HOSTNAME: machine hostname, default to the output of 'hostname' command
    COLLECTD_INTERVAL: script loop run interval, default to 60 seconds

#### Sample output ####

    PUTVAL "mymachine.domain.local/top-cpu/gauge-firefox" interval=60 N:0.5
    PUTVAL "mymachine.domain.local/top-cpu/gauge-usr_libexec_Xorg" interval=60 N:0.5
    PUTVAL "mymachine.domain.local/top-mem/gauge-firefox" interval=60 N:644308
    PUTVAL "mymachine.domain.local/top-mem/gauge-usr_bin_python3" interval=60 N:150696

#### Loading the script via collecd configuration ####

    <Plugin exec>
      Exec "nobody" "/path/to/top.sh" ["-i INT"] ["-d INT"] ["-t INT"]
    </Plugin>

Remember to `chmod +x` it.

#### Grafana example preview ####

![grafana graph](./top-cpu.png)

#### Similar extensions ####

- via python plugin: [collectd-top](https://github.com/gaurapanasenko/collectd-top/)
- via perl plugin: [collectd-top](https://github.com/michelgokan/collectd-top/)

____
____

