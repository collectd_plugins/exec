#!/usr/bin/env bash

HOSTNAME="${COLLECTD_HOSTNAME:-$(hostname)}"
INTERVAL="${COLLECTD_INTERVAL:-60}"

iter=2
delay=2
tasks=10

do_top() {
    local met="${1}"
    if [[ "${met}" == "CPU" ]] ; then
        idx="9"
    elif [[ "${met}" == "MEM" ]] ; then
        idx="6"
    fi
    top -c -b -n "${iter}" -d "${delay}" -o "+%${met}" -w 128 \
    | grep -A $(( tasks + 6 )) -E '^top -' \
    | tail -n "${tasks}" \
    | awk -v idx="${idx}" '
        {
            gsub(/[^0-9a-zA-Z_-]/, "_", $12) ;
            gsub(/^_/, "", $12) ;
            gsub(/_$/, "", $12) ;
            if ( idx == "6" ) {
                if ( $idx ~ /g$/ ) {
                    gsub(/g$/, "", $idx) ;
                    $idx = $idx*1024 ;
                } else {
                    $idx = $idx/1024 ;
                }
            }
            {
                a[$12] += $idx ;
            }
        }
        END {
            for (i in a) {
                if (a[i] != "0") {
                    print i";"a[i]
                }
            }
        }
    '
}

while getopts "i:d:t:" c ; do
    case $c in
        i)	iter=$OPTARG;;
        d)	delay=$OPTARG;;
        t)	tasks=$OPTARG;;
    esac
done

while sleep $( bc -l <<< "${INTERVAL} - ( ${iter} * ${delay} ) - 1" ) ; do
    readarray -t clist < <( do_top "CPU" )
    for citem in "${clist[@]}" ; do
        IFS=";" read -r ccommand cvalue dirt <<<"${citem}"
        echo "PUTVAL \"${HOSTNAME}/top-cpu/gauge-${ccommand}\" interval=${INTERVAL} N:${cvalue}"
    done
    readarray -t mlist < <( do_top "MEM" )
    for mitem in "${mlist[@]}" ; do
        IFS=";" read -r mcommand mvalue dirt <<<"${mitem}"
        echo "PUTVAL \"${HOSTNAME}/top-mem/gauge-${mcommand}\" interval=${INTERVAL} N:${mvalue}"
    done
done

